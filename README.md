# 如何在 WordPress 中使用 jQuery

1. 建立並啟用子主題
2. 修改 functions.php
3. 建立 /js/myscripts.js


## 如何建立子主題

### 手動建立

* 在本機電腦內建立資料夾，命名方式通常為 "(主題)-Child"，例如請建立 "twentyseventeen-child" 資料夾
* 在 twentyseventeen-child 資料夾內建立 style.css 檔，內容如下：

##### style.css 內容如下(請特別注意 "Template" 填入父主題名稱)：

<pre><code>/*
Theme Name:		Twenty Seventeen Child
Theme URI:		https://wordpress.org/themes/twentyseventeen/
Template:		twentyseventeen
Author:			the WordPress team
Author URI:		https://wordpress.org/
Description:	This is description.
Tags: 			tags
Version:		0.1.0
Updated:		2018-03-15 15:42:15
*/</code></pre>

* 在 twentyseventeen-child 資料夾內建立 functions.php 檔，內容如下，請把 parent-style 改成你的父主題名稱：

##### functions.php 的內容如下，請把 'parent-style' 改成你的父主題名稱：

<pre><code>
&lt;?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
</code></pre>

* 將 twentyseventeen-child 整個資料夾上傳到主機的 wp-content/themes/ 內
* 回到 WordPress 的 外觀 -> 佈景主題 -> 啟動子主題

### 使用外掛建立子主題

* 安裝外掛 "Child Theme Configurator" 並建立、啟用子主題

## 修改 functions.php 來載入你的 jQuery 檔

* 進入 WordPress 後台，點選 外觀 -> 主題編輯器，右側 "選取要編輯的佈景主題" 的下拉選單選擇子主題，按 "選取"
* 查看下方是否有 functions.php 檔，如果沒有，請用 FTP 登入主機後在 /wp-content/themes/(你的子主題)/ 內建立一個 function.php 的文字檔即可。
* 點選 funcitons.php 在尾端加入以下內容：

<pre><code>function load_page_script() {
	// 加入 JS
	$custom_scriptsrc = get_stylesheet_directory_uri().'/js/';
	wp_register_script('custom-index-js', $custom_scriptsrc . 'myscripts.js', 'jquery', false, false);
	
	// 加入 CSS，如果要加入自訂的 CSS，請把註解拿掉
	//$custom_stylesrc = get_stylesheet_directory_uri(). '/css/';
	//wp_register_style('custom-woocommerce-checkout', $custom_stylesrc. 'some.css' );
    
	wp_enqueue_script('jquery');
	wp_enqueue_script('custom-index-js');
	//wp_enqueue_script('custom-index-page');
    
}
add_action('wp_enqueue_scripts', 'load_page_script');
</code></pre>

## 加入你的 jQuery 檔

* 用 FTP 登入主機，在 /wp-content/themes/twentyseventeen-child/ 內建立 js 資料夾，然後在建立 myscripts.js 檔
* 回到 外觀 -> 主題編輯器，選取您的子主題後，因該會看到一個 js 目錄，展開後有 myscripts.js ，編輯他就可以了。

<pre><code>
jQuery(document).ready(function($){
	// Your jQuery Codes here
    alert('Hello World');
});
</code></pre>

## FAQ

### 到底要建立什麼檔案？
最終，你應該有一樣的檔案結構(twentyseventeen-child 替換成你的子主題名稱)：
<pre><code>
twentyseventeen-child/
 +- style.css
 +- functions.php
 +- js/
      +- myscripts.js 
</code></pre>

### jQuery 不會動

* 請在首頁上按右鍵，找找看有沒有載入 myscripts.js 檔，如果沒有，表示你的 functions.php 有誤
* 在首頁上按 F12，可以開啟瀏覽器的除錯工具，看看有沒有錯誤訊息

