<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

function load_page_script() {
	// 加入 JS
	$custom_scriptsrc = get_stylesheet_directory_uri().'/js/';
	wp_register_script('custom-index-js', $custom_scriptsrc . 'myscripts.js', 'jquery', false, false);
	
	// 加入 CSS
	//$custom_stylesrc = get_stylesheet_directory_uri(). '/css/';
	//wp_register_style('custom-woocommerce-checkout', $custom_stylesrc. 'some.css' );
    
	wp_enqueue_script('jquery');
	wp_enqueue_script('custom-index-js');
	//wp_enqueue_script('custom-index-page');

/*
	if ( is_page(7) ){
		wp_enqueue_scipt('custom-shop-page');
		wp_enqueue_style('custom-woocommerce-checkout');
	}
*/
}
add_action('wp_enqueue_scripts', 'load_page_script');

// END ENQUEUE PARENT ACTION
